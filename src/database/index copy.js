const MongoDB = require('./MongoDB');

const connections = {};

function createConnection() {
    const deferred = Q.defer();
    const database = 'getir-case-study';
    if (!connections[database]) {
        const mongodbInstance = new MongoDB({ database: 'getir-case-study'});
        mongodbInstance.connect()
            .then(() => {
                connections[database] = mongodbInstance.connection;
                return true;
            })
            .catch((err) => err);
    } else {
        retu
    }
    return deferred.promise;
}

/**
 * @function getConnection
 * @description This function creates multiple database connections and stores them.
 * It uses the connection string and connection options to create a connection.
 * @param {String} databaseName
 * @return *
 * @throws {Error} if database connection doesn't exist.
 */
function getDatabaseConnection(databaseName) {
    if (!connections[databaseName]) {
        throw new Error(`No ${databaseName} connection created.`);
    }
    return connections[databaseName];
}

module.exports = {
    createConnection,
    getDatabaseConnection,
};
