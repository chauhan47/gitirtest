const { MongoClient } = require('mongodb');
const config = require('../../config');

const client = new MongoClient(config.uri || 'mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true');

const connections = {};

module.exports = {
  init: async () => {
    try {
      await client.connect();
    } catch (e) {
      console.error("connnection err", e);
    }
  },
  connections,
  findRecords: async (filter) => {
    const database = client.db('getir-case-study');
    const collection = database.collection('records');
    const recordsCursor = await collection.find(filter)
    // const recordsCursor = await collection.aggregate([
    //   {
    //     // createdAt: { $gte: new Date(req.body.startDate), $lt: new Date(req.body.endDate) }
    //     $project: {
    //       createdAt: {
    //         $filter: {
    //           input: "$createdAt",
    //           as: "date",
    //           cond: { $gte: new Date(2014-01-01), $lt: new Date(2016-01-01) }
    //         }
    //       },
    //       key: '$key',
    //       totalCount: { $sum: "$counts" },
    //       // qtyGte250: { $gte: [{ $sum: "$counts" }, 3000] },
    //     }
    //   }
    // ]);
    const records = await recordsCursor.toArray()
    return records;
  }

};