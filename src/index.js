const express = require('express');
const cors = require('cors');
const routes = require('./routes');
const db = require('./database');
const config = require('../config');

(async () => {
	try {
		await db.init();
		console.log("Db connected successfully!");
	} catch (error) {
		console.log("Db connection failed with error", error);
	}
})();

const app = express();
app.use(express.json({ limit: '200mb' }));
app.use(express.urlencoded({ limit: '200mb', extended: true }));
app.use(express.static('./src/public'));

// cors for cookies
const corsOptions = {
	credentials: true,
	origin: []
};

//middlewares
app.use(cors(corsOptions));

//routes
app.use('/', routes);

process.on('uncaughtException', (error) => {
    console.log(error, 'Uncaught Exception');
    process.exit(1000);
});
process.on('unhandledRejection', (error) => {
    console.log(error, 'Unhandled Rejection');
    process.exit(1000);
});


app.listen(config.port || process.env.PORT || 3000, () => {
	console.log(`server running ${config.port}`);
});

