
const sendResponse = (req, res, result) => {
    const responseObject = {
        status: 200,
        result: {
            statusCode: 200,
            data: result,
        },
    };
    res.status(responseObject.status);
    res.send(responseObject.result);
};

const sendError = (req, res, error) => {
    const result = error.getErrorObject ? error.getErrorObject() : {
        code: error.code || 500,
    };
    let errorMessage = 'Ops! Something went wrong';
    if (error.message) {
        errorMessage = error.message;
    }
    result.error_message = errorMessage;
    const responseObject = {
        status: error.statusCode || 500,
        result,
    };
    if (responseObject.status >= 500) {
        req.logger.error(error);
    } else {
        req.logger.warn(error);
    }
    res.status(500);
    res.send(responseObject.result);
};

module.exports = (err, req, res, next) => {
    if (err instanceof Error) {
        sendError(req, res, err);
    } else {
        sendResponse(req, res, err);
    }
    next();
};
