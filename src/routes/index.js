const express = require('express');
const router = express.Router();
const { ResponseHandler } = require('../middleware');

router.get('/', (req, res) => {
    res.send({
        success: true,
    });
});

router.use('/api/records',  require('./dataRoutes'))

router.use(ResponseHandler);

module.exports = router;